from typing import Any
from typing import Optional

from pydantic import BaseModel


class BaseResponseSchema(BaseModel):
    success: bool
    message: Optional[str]
    data: Optional[dict]
