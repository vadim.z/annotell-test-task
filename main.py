import json
import logging

from converter import to_open_label_annotation
from fastapi import FastAPI
from fastapi import Query

from schemas import BaseResponseSchema

app = FastAPI()
logger = logging.getLogger(__name__)


@app.get('/convert', response_model=BaseResponseSchema)
def convert_to_open_label(
    data: str = Query(..., title='Annotell annotation data', description='Annotell annotation JSON data.')
):
    try:
        return {
            'success': True,
            'data': to_open_label_annotation(json.loads(data))
        }
    except:  # NOQA
        logger.exception(f'Failed to process Annotell annotation: {data}.')
        return {
            'success': False,
            'message': 'Failed to convert data.'
        }
