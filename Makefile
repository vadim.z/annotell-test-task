.PHONY: help deploy run

help: ## display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

deploy: ## install
	test -d .venv || virtualenv .venv --python=python3
	. .venv/bin/activate; pip install --upgrade --requirement requirements.txt

run: # run local server
	. .venv/bin/activate; uvicorn main:app --reload
